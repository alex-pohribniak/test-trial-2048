using UnityEngine;
using UnityEngine.SceneManagement;

namespace MenuScene.Logic
{
    public class MenuLogic : MonoBehaviour
    {
        public void LoadGameScene()
        {
            SceneManager.LoadSceneAsync("Scenes/GameScene");
        }
    }
}