﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Singletons
{
    public class Observer : MonoBehaviour
    {
        private static Observer _observer;
        private Dictionary<string, Dictionary<int, Delegate>> _listeners;

        private static Observer Instance
        {
            get
            {
                if (_observer != null) return _observer;
                _observer = FindObjectOfType(typeof(Observer)) as Observer;
                if (_observer == null)
                    throw new Exception("There needs to be one active Observer script in your scene.");
                _observer.Init();
                return _observer;
            }
        }

        private void Init()
        {
            _listeners = new Dictionary<string, Dictionary<int, Delegate>>();
        }

        public static void Clear()
        {
            Instance._listeners.Clear();
        }

        public static bool IsNull()
        {
            return _observer == null;
        }

        private static void AddListener(string eventName, Delegate listener)
        {
            var listeners = Instance._listeners;
            if (listeners.ContainsKey(eventName))
            {
                var delegates = listeners[eventName];
                if (delegates == null)
                {
                    delegates = new Dictionary<int, Delegate> {{listener.GetHashCode(), listener}};
                    listeners[eventName] = delegates;
                }
                else if (!delegates.ContainsKey(listener.GetHashCode()))
                {
                    delegates.Add(listener.GetHashCode(), listener);
                }
            }
            else
            {
                var delegates = new Dictionary<int, Delegate>
                {
                    {listener.GetHashCode(), listener}
                };
                listeners.Add(eventName, delegates);
            }
        }

        private static void RemoveListener(string eventName, Delegate listener)
        {
            var listeners = Instance._listeners;
            if (!listeners.ContainsKey(eventName)) return;
            var delegates = listeners[eventName];
            if (delegates == null || !delegates.ContainsKey(listener.GetHashCode())) return;
            if (delegates.Remove(listener.GetHashCode()) && delegates.Count == 0)
                listeners.Remove(eventName);
        }

        public static void Emit(string eventName, params object[] args)
        {
            var listeners = Instance._listeners;
            var handlers = new List<Delegate>();
            foreach (var listener in listeners.Where(listener => listener.Key == eventName))
                handlers.AddRange(listener.Value.Values.ToList());
            foreach (var handler in handlers) handler?.DynamicInvoke(args);
        }

        public static void AddListener(string eventName, Action listener)
        {
            AddListener(eventName, (Delegate) listener);
        }

        public static void RemoveListener(string eventName, Action listener)
        {
            RemoveListener(eventName, (Delegate) listener);
        }

        //Action with type example 
        /*public static void AddListener(string eventName, Action<Color> listener)
        {
            AddListener(eventName, (Delegate) listener);
        }
        public static void RemoveListener(string eventName, Action<Color> listener)
        {
            RemoveListener(eventName, (Delegate) listener);
        }*/
    }
}