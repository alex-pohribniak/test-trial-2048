using System;
using GameScene.Models;
using UnityEngine;

namespace Common.Singletons
{
    public class Model : MonoBehaviour
    {
        private static Model _model;
        private GameData _gameData;

        private static Model Instance
        {
            get
            {
                if (_model != null) return _model;
                _model = FindObjectOfType(typeof(Model)) as Model;
                if (_model == null)
                    throw new Exception("There needs to be one active Model script in your scene.");
                _model.Init();

                return _model;
            }
        }

        private void Init()
        {
            _gameData ??= new GameData();
        }

        public static GameData GameData
        {
            get => Instance._gameData;
            set => Instance._gameData = value;
        }
    }
}