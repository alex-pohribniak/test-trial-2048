﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Utils
{
    public static class DelayUtils
    {
        private static MonoBehaviour _monoBehaviour;
        
       
        public static Coroutine DelayedCall(float delay, Action action)
        {
            if (_monoBehaviour == null)
                _monoBehaviour = new GameObject().AddComponent<Image>();
            
            return _monoBehaviour.StartCoroutine(ExecuteDelay(delay, action));
        }
        
        public static void StopDelayedCall(Coroutine coroutine)
        {
            if (_monoBehaviour != null) _monoBehaviour.StopCoroutine(coroutine);
        }
        
        private static IEnumerator ExecuteDelay(float delay, Action action)
        {
            yield return new WaitForSecondsRealtime(delay);
            action.Invoke();
        }
    }
}
