﻿namespace Common.Enums
{
    public static class GameEvents
    {
        public const string InitObjectPool = "InitObjectPool";
        public const string InitTilesView = "InitTilesView";

        public const string UpdateInitialTileView = "UpdateInitialTileView";

        public const string LockControl = "LockControl";
        public const string UnLockControl = "UnLockControl";

        public const string UpdateTilesView = "UpdateTilesView";

        public const string TilesAnimation = "TilesAnimation";
        public const string TilesAnimationComplete = "TilesAnimationComplete";

        public const string InitialTilesAnimation = "InitialTilesAnimation";
        public const string InitialTilesAnimationComplete = "InitialTilesAnimationComplete";
        
        public const string GoalReached = "GoalReached";
        public const string GameOver = "GameOver";
        
    }
}