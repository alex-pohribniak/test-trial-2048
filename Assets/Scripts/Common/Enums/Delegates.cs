using Common.Tasks;
using GameScene.Tasks;

namespace Common.Enums
{
    public delegate void TaskDelegate(BaseTask task);
}