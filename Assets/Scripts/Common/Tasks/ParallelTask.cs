﻿using System.Collections.Generic;
using Common.Enums;
using GameScene.Tasks;

namespace Common.Tasks
{
    public class ParallelTask : BaseTask
    {
        private int _taskCompleteCount;
        private int _taskCount;
        private List<BaseTask> _tasks;
        
        public ParallelTask(List<BaseTask> tasks)
        {
            TaskDelegate completeDelegate = OnChildTaskComplete;
            TaskDelegate guardDelegate = OnChildTaskGuard;
            _tasks = tasks;
            _taskCount = _tasks.Count;
            _taskCompleteCount = 0;
            for (var i = 0; i < _taskCount; i++)
            {
                var task = _tasks[i];
                task.onTaskComplete = completeDelegate;
                task.onTaskGuard = guardDelegate;
            }
        }

        protected override void DeActivate()
        {
            _taskCompleteCount = 0;
            for (var i = 0; i < _taskCount; i++) _tasks[i].forceMode = false;

            base.DeActivate();
        }

        protected virtual void OnChildTaskComplete(BaseTask task)
        {
            _taskCompleteCount++;
            if (_taskCompleteCount == _taskCount) Complete();
        }

        private void OnChildTaskGuard(BaseTask task)
        {
            _taskCompleteCount++;
            if (_taskCompleteCount == _taskCount) Complete();
        }

        protected override void OnForceExecute()
        {
            OnExecute();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            _taskCompleteCount = 0;
            for (var i = 0; i < _taskCount; i++) _tasks[i].Execute();
        }

        public override void ForceModeOn()
        {
            for (var i = 0; i < _taskCount; i++) _tasks[i].ForceModeOn();
            base.ForceModeOn();
        }
        
        public override void ForceModeOff()
        {
            for (var i = 0; i < _taskCount; i++) _tasks[i].ForceModeOff();
            base.ForceModeOff();
        }

        protected internal override void OnTerminate()
        {
            for (var i = 0; i < _taskCount; i++) _tasks[i].Terminate();
            base.OnTerminate();
        }
    }
}