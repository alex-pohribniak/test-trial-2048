using Common.Utils;
using GameScene.Tasks;
using UnityEngine;

namespace Common.Tasks
{
    public class DelaySecondsTask : BaseTask
    {
        private readonly float _seconds;
        private Coroutine _delayCoroutine;

        public DelaySecondsTask(float seconds)
        {
            _seconds = seconds;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            _delayCoroutine = DelayUtils.DelayedCall(_seconds, Complete);
        }

        protected internal override void OnTerminate()
        {
            DelayUtils.StopDelayedCall(_delayCoroutine);
            base.OnTerminate();
        }
    }
}