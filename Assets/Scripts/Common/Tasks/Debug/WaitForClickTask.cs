﻿using Common.Tasks;
using GameScene.Tasks;
using UnityEngine;

namespace Tasks.Debug
{
    public class WaitForClickTask : BaseTask
    {
        private void Update()
        {
            if (InProgress && Input.GetMouseButtonDown(0))
                Complete();
        }
    }
}