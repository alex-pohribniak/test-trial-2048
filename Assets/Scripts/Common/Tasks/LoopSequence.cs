﻿using System.Collections.Generic;
using GameScene.Tasks;
using Tasks;

namespace Common.Tasks
{
    public class LoopSequence : SequenceTask
    {
        public LoopSequence(List<BaseTask> tasks) : base(tasks)
        {
        }

        public override void Complete()
        {
            InProgress = false;
            Execute();
        }
    }
}