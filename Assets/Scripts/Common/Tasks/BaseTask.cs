﻿using Common.Enums;
using UnityEngine;

namespace Common.Tasks
{
    public class BaseTask
    {
        private float _startTime;
        protected internal bool forceMode;
        public bool logging;
        public TaskDelegate onTaskComplete;
        public TaskDelegate onTaskGuard;
        public TaskDelegate onTaskTerminate;
        public object context;
        
        public virtual bool InProgress { get; protected set; }

        public virtual void ForceModeOn()
        {
            forceMode = true;
            if (logging)
            {
                // print($"{name}: force mode ON, time: {Time.time - _startTime}s");
                // Observer.Emit(CommonEvent.Logging, $"{name}: force mode ON, time: {Time.time - _startTime}s");
            }

            if (InProgress) ForceComplete();
        }

        public virtual void ForceModeOff()
        {
            forceMode = false;
            if (!logging) return;
            // print($"{name}: force mode OFF, time: {Time.time - _startTime}s");
            // Observer.Emit(CommonEvent.Logging, $"{name}: force mode OFF, time: {Time.time - _startTime}s");
        }

        protected virtual void Activate()
        {
            if (logging)
                _startTime = Time.time;
            // print(forceMode ? $"{name}: activate force" : $"{name}: activate");
            // Observer.Emit(CommonEvent.Logging, forceMode ? $"{name}: activate force" : $"{name}: activate");

            InProgress = true;
        }

        protected virtual void DeActivate()
        {
            if (logging)
            {
                // print($"{name}: de_activate, life time: {Time.time - _startTime}s");
                // Observer.Emit(CommonEvent.Logging, $"{name}: de_activate, life time: {Time.time - _startTime}s");
            }

            InProgress = false;
            forceMode = false;
        }

        protected virtual void ForceComplete()
        {
            Complete();
        }

        public void Execute()
        {
            if (InProgress) return;
            if (Guard())
            {
                if (forceMode)
                    OnForceExecute();
                else
                    OnExecute();
            }
            else
            {
                OnGuard();
            }
        }

        protected virtual bool Guard()
        {
            return true;
        }

        protected virtual void OnGuard()
        {
            if (logging)
            {
                // print($"{name}: on_guard");
                // Observer.Emit(CommonEvent.Logging, $"{name}: on_guard");
            }

            DeActivate();
            onTaskGuard?.DynamicInvoke(this);
        }

        public virtual void Complete()
        {
            DeActivate();
            onTaskComplete?.DynamicInvoke(this);
        }

        public void Terminate()
        {
            if (InProgress) OnTerminate();
        }

        protected internal virtual void OnTerminate()
        {
            DeActivate();
            onTaskTerminate?.DynamicInvoke(this);
        }

        protected virtual void OnExecute()
        {
            Activate();
        }

        protected virtual void OnForceExecute()
        {
            OnExecute();
        }
    }
}