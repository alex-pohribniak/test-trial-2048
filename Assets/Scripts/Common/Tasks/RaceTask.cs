using System.Collections.Generic;
using GameScene.Tasks;

namespace Common.Tasks
{
    public class RaceTask: ParallelTask
    {
        protected override void OnChildTaskComplete(BaseTask task)
        {
            Complete();
        }

        public RaceTask(List<BaseTask> tasks) : base(tasks)
        {
        }
    }
}