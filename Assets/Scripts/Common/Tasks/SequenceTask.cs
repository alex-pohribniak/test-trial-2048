﻿using System.Collections.Generic;
using Common.Enums;
using GameScene.Tasks;
using JetBrains.Annotations;

namespace Common.Tasks
{
    public class SequenceTask : BaseTask
    {
        private BaseTask _currentTask;
        private int _currentTaskIndex;
        private int _taskCount;
        private List<BaseTask> _tasks;

        public SequenceTask([CanBeNull] List<BaseTask> tasks)
        {
            TaskDelegate completeDelegate = OnChildTaskComplete;
            TaskDelegate guardDelegate = OnChildTaskGuard;
            _tasks = tasks;
            _taskCount = _tasks.Count;
            for (var i = 0; i < _taskCount; i++)
            {
                var task = _tasks[i];
                task.onTaskComplete = completeDelegate;
                task.onTaskGuard = guardDelegate;
            }
        }

        protected override void DeActivate()
        {
            _currentTask = null;
            _currentTaskIndex = -1;
            for (var i = 0; i < _taskCount; i++) _tasks[i].forceMode = false;

            base.DeActivate();
        }

        private void OnChildTaskComplete(BaseTask task)
        {
            if (_currentTask == task) NextTask();
        }

        private void OnChildTaskGuard(BaseTask task)
        {
            if (_currentTask == task) NextTask();
        }

        protected override void OnForceExecute()
        {
            OnExecute();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            _currentTask = null;
            _currentTaskIndex = -1;
            NextTask();
        }

        private void NextTask()
        {
            _currentTaskIndex++;
            if (_currentTaskIndex < _taskCount)
            {
                _currentTask = _tasks[_currentTaskIndex];
                _currentTask.context = context;
                _currentTask.Execute();
            }
            else
            {
                Complete();
            }
        }

        public override void ForceModeOn()
        {
            for (var i = 0; i < _taskCount; i++) _tasks[i].ForceModeOn();
            base.ForceModeOn();
        }

        public override void ForceModeOff()
        {
            for (var i = 0; i < _taskCount; i++) _tasks[i].ForceModeOff();
            base.ForceModeOff();
        }

        protected internal override void OnTerminate()
        {
            if (_currentTask != null) _currentTask.OnTerminate();
            base.OnTerminate();
        }
    }
}