﻿using System.Collections.Generic;
using Common.Enums;
using Common.Singletons;
using UnityEngine;

namespace GameScene.View
{
    public class ObjectPool : MonoBehaviour
    {
        public static ObjectPool SharedInstance;
        public List<GameObject> pooledObjects;
        public GameObject objectToPool;
        public Transform parentForStatic;
        public int amountToPool;

        private void Awake()
        {
            SharedInstance = this;
            Observer.AddListener(GameEvents.InitObjectPool, OnInitObjectPool);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvents.InitObjectPool, OnInitObjectPool);
        }

        private void OnInitObjectPool()
        {
            pooledObjects = new List<GameObject>();
            for (var i = 0; i < amountToPool; i++)
            {
                var tmp = Instantiate(objectToPool, parentForStatic);
                tmp.SetActive(false);
                pooledObjects.Add(tmp);
            }
        }

        public GameObject GetPooledObject()
        {
            for (var i = 0; i < amountToPool; i++)
                if (!pooledObjects[i].activeInHierarchy)
                    return pooledObjects[i];
            return null;
        }
    }
}