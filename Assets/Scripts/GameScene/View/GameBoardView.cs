﻿using System;
using System.Collections.Generic;
using Common.Enums;
using Common.Singletons;
using DG.Tweening;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    public class GameBoardView : MonoBehaviour
    {
        public Transform parentForDynamic;
        public GridLayoutGroup gridLayoutGroup;
        public Controller.Controller controller;
        public GameObject endGameObj;
        public GameObject goalReachedObj;
        public GameObject gameOverObj;
        public Dictionary<Vector2Int, TileView> StaticTilesView;

        private void Awake()
        {
            Observer.AddListener(GameEvents.InitTilesView, OnInitTilesView);
            Observer.AddListener(GameEvents.UpdateInitialTileView, OnUpdateInitialTileView);
            Observer.AddListener(GameEvents.LockControl, OnLockControl);
            Observer.AddListener(GameEvents.UnLockControl, OnUnLockControl);
            Observer.AddListener(GameEvents.UpdateTilesView, OnUpdateTilesView);
            Observer.AddListener(GameEvents.TilesAnimation, OnTilesAnimation);
            Observer.AddListener(GameEvents.InitialTilesAnimation, OnInitialTilesAnimation);
            Observer.AddListener(GameEvents.GoalReached, OnGoalReached);
            Observer.AddListener(GameEvents.GameOver, OnGameOver);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvents.InitTilesView, OnInitTilesView);
            Observer.RemoveListener(GameEvents.UpdateInitialTileView, OnUpdateInitialTileView);
            Observer.RemoveListener(GameEvents.LockControl, OnLockControl);
            Observer.RemoveListener(GameEvents.UnLockControl, OnUnLockControl);
            Observer.RemoveListener(GameEvents.UpdateTilesView, OnUpdateTilesView);
            Observer.RemoveListener(GameEvents.TilesAnimation, OnTilesAnimation);
            Observer.RemoveListener(GameEvents.InitialTilesAnimation, OnInitialTilesAnimation);
            Observer.RemoveListener(GameEvents.GoalReached, OnGoalReached);
            Observer.RemoveListener(GameEvents.GameOver, OnGameOver);
        }

        private void OnGameOver()
        {
            endGameObj.SetActive(true);
            gameOverObj.SetActive(true);
        }

        private void OnGoalReached()
        {
            endGameObj.SetActive(true);
            goalReachedObj.SetActive(true);
        }

        private void OnInitialTilesAnimation()
        {
            var animationSequence = DOTween.Sequence();
            foreach (var animationSequenceData in Model.GameData.AnimationsSequenceData)
            {
                var animationSubSequence = DOTween.Sequence();
                foreach (var animationData in animationSequenceData.Value)
                {
                    var startStaticTileView = StaticTilesView[animationData.StartPosition];
                    startStaticTileView.tileInnerImage.color =
                        GameConfig.ValueColorDictionary[GameConfig.InitialTileValue];
                    startStaticTileView.tileValue.gameObject.SetActive(false);
                    startStaticTileView.tileValue.text = GameConfig.InitialTileValue.ToString();

                    var tileClone = ObjectPool.SharedInstance.GetPooledObject();
                    if (tileClone.TryGetComponent(out TileView tileView))
                    {
                        if (tileView.TryGetComponent(out RectTransform rectTransform))
                        {
                            rectTransform.SetParent(parentForDynamic);
                            rectTransform.anchorMin = Vector2.up;
                            rectTransform.anchorMax = Vector2.up;
                            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                                gridLayoutGroup.cellSize.x);
                            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                                gridLayoutGroup.cellSize.y);
                            var startStaticTileViewTransform = startStaticTileView.transform;
                            rectTransform.SetPositionAndRotation(startStaticTileViewTransform.position,
                                startStaticTileViewTransform.rotation);
                        }

                        tileClone.transform.localScale = Vector3.zero;
                        tileClone.SetActive(true);

                        tileView.tileInnerImage.color = animationData.StartColor;
                        tileView.tileValue.gameObject.SetActive(true);
                        tileView.tileValue.text = animationData.StartValue.ToString();

                        var tweenScaleTarget = tileClone.transform.DOScale(Vector3.one, GameConfig.AnimationDuration);
                        tweenScaleTarget.onComplete = () =>
                        {
                            tileView.tileInnerImage.color = animationData.TargetColor;
                            tileView.tileValue.gameObject.SetActive(true);
                            tileView.tileValue.text = animationData.TargetValue.ToString();
                            tileClone.SetActive(false);
                        };
                        animationSubSequence.Append(tweenScaleTarget);
                    }
                    else
                    {
                        return;
                    }
                }

                animationSequence.Join(animationSubSequence);
            }

            animationSequence.onComplete = () =>
            {
                Observer.Emit(GameEvents.InitialTilesAnimationComplete);
                animationSequence.Kill();
                animationSequence = null;
            };
            animationSequence.Play();
        }

        private void OnTilesAnimation()
        {
            var animationSequence = DOTween.Sequence();
            foreach (var animationSequenceData in Model.GameData.AnimationsSequenceData)
            {
                var animationSubSequence = DOTween.Sequence();
                foreach (var animationData in animationSequenceData.Value)
                {
                    var startStaticTileView = StaticTilesView[animationData.StartPosition];

                    var tileClone = ObjectPool.SharedInstance.GetPooledObject();
                    if (tileClone.TryGetComponent(out TileView tileView))
                    {
                        if (tileView.TryGetComponent(out RectTransform rectTransform))
                        {
                            rectTransform.SetParent(parentForDynamic);
                            rectTransform.anchorMin = Vector2.up;
                            rectTransform.anchorMax = Vector2.up;
                            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                                gridLayoutGroup.cellSize.x);
                            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                                gridLayoutGroup.cellSize.y);
                            var startStaticTileViewTransform = startStaticTileView.transform;
                            rectTransform.SetPositionAndRotation(startStaticTileViewTransform.position,
                                startStaticTileViewTransform.rotation);
                        }

                        tileView.tileOuterImage.enabled = false;
                        tileView.tileInnerImage.enabled = false;
                        tileView.tileValue.enabled = false;
                        tileClone.SetActive(true);

                        var targetStaticTileView = StaticTilesView[animationData.TargetPosition];

                        var tweenMove = tileClone.transform.DOMove(targetStaticTileView.transform.position,
                            GameConfig.AnimationDuration);
                        tweenMove.onPlay = () =>
                        {
                            tileView.tileOuterImage.enabled = true;
                            tileView.tileInnerImage.enabled = true;
                            tileView.tileValue.enabled = true;
                            switch (animationData.AnimationType)
                            {
                                case AnimationType.Move:
                                    tileView.tileInnerImage.color = animationData.StartColor;
                                    tileView.tileValue.gameObject.SetActive(true);
                                    tileView.tileValue.text = animationData.StartValue.ToString();
                                    break;
                                case AnimationType.Merge:
                                    tileView.tileInnerImage.color = animationData.TargetColor;
                                    tileView.tileValue.gameObject.SetActive(false);
                                    tileView.tileValue.text = animationData.TargetValue.ToString();
                                    break;
                                case AnimationType.UpdateInitialTile:
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                            startStaticTileView.tileInnerImage.color =
                                GameConfig.ValueColorDictionary[GameConfig.InitialTileValue];
                            startStaticTileView.tileValue.gameObject.SetActive(false);
                            startStaticTileView.tileValue.text = GameConfig.InitialTileValue.ToString();
                        };
                        tweenMove.onComplete = () =>
                        {
                            tileView.tileInnerImage.color = animationData.TargetColor;
                            tileView.tileValue.gameObject.SetActive(true);
                            tileView.tileValue.text = animationData.TargetValue.ToString();
                            if (animationData.AnimationType == AnimationType.Move)
                            {
                                tileClone.SetActive(false);
                                targetStaticTileView.tileInnerImage.color = animationData.TargetColor;
                                targetStaticTileView.tileValue.gameObject.SetActive(true);
                                targetStaticTileView.tileValue.text = animationData.TargetValue.ToString();
                            }
                        };
                        animationSubSequence.Join(tweenMove);

                        if (animationData.AnimationType != AnimationType.Merge) continue;
                        var tweenShakeScale =
                            tileClone.transform.DOShakeScale(GameConfig.AnimationDuration, 0.2f);
                        tweenShakeScale.onComplete = () =>
                        {
                            tileClone.SetActive(false);
                            targetStaticTileView.tileInnerImage.color = animationData.TargetColor;
                            targetStaticTileView.tileValue.gameObject.SetActive(true);
                            targetStaticTileView.tileValue.text = animationData.TargetValue.ToString();
                        };
                        animationSubSequence.Append(tweenShakeScale);
                    }
                    else
                    {
                        return;
                    }
                }

                animationSequence.Append(animationSubSequence);
            }

            animationSequence.onComplete = () =>
            {
                Observer.Emit(GameEvents.TilesAnimationComplete);
                animationSequence.Kill();
                animationSequence = null;
            };
            animationSequence.Play();
        }

        private void OnUpdateTilesView()
        {
            for (var i = 0; i < GameConfig.GameBoardRows; i++)
            for (var j = 0; j < GameConfig.GameBoardColumns; j++)
            {
                var position = new Vector2Int(i, j);
                if (StaticTilesView[position].tileValue.text != Model.GameData.TilesData[position].TileValue.ToString())
                {
                    StaticTilesView[position].tileInnerImage.color =
                        GameConfig.ValueColorDictionary[Model.GameData.TilesData[position].TileValue];
                    StaticTilesView[position].tileValue.text = Model.GameData.TilesData[position].TileValue.ToString();
                    StaticTilesView[position].tileValue.gameObject.SetActive(StaticTilesView[position].tileValue.text !=
                                                                             GameConfig.InitialTileValue.ToString());
                }
            }
        }

        private void OnUnLockControl()
        {
            controller.enabled = true;
        }

        private void OnLockControl()
        {
            controller.enabled = false;
        }

        private void OnUpdateInitialTileView()
        {
            for (var i = 0; i < GameConfig.GameBoardRows; i++)
            for (var j = 0; j < GameConfig.GameBoardColumns; j++)
            {
                var position = new Vector2Int(i, j);
                if (StaticTilesView[position].tileValue.text ==
                    Model.GameData.TilesData[position].TileValue.ToString()) continue;
                StaticTilesView[position].tileInnerImage.color =
                    GameConfig.ValueColorDictionary[Model.GameData.TilesData[position].TileValue];
                StaticTilesView[position].tileValue.text = Model.GameData.TilesData[position].TileValue.ToString();
                StaticTilesView[position].tileValue.gameObject.SetActive(true);
            }
        }

        private void OnInitTilesView()
        {
            StaticTilesView = new Dictionary<Vector2Int, TileView>();
            foreach (var tileData in Model.GameData.TilesData)
            {
                var tileClone = ObjectPool.SharedInstance.GetPooledObject();
                if (!tileClone.TryGetComponent(out TileView tileView)) continue;
                tileView.tileInnerImage.color = GameConfig.ValueColorDictionary[tileData.Value.TileValue];
                tileView.tileValue.text = tileData.Value.TileValue.ToString();
                tileView.gameObject.SetActive(true);
                StaticTilesView.Add(tileData.Key, tileView);
            }
        }
    }
}