﻿using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    public class TileView : MonoBehaviour
    {
        public Image tileOuterImage;
        public Image tileInnerImage;
        public Text tileValue;
    }
}