﻿using Common.Enums;
using Common.Singletons;
using Common.Tasks;

namespace GameScene.Tasks
{
    public class LockControlTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvents.LockControl);
            Complete();
        }
    }
}