﻿using Common.Enums;
using Common.Singletons;
using Common.Tasks;

namespace GameScene.Tasks
{
    public class InitialTilesAnimationTask : BaseTask
    {
        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvents.InitialTilesAnimationComplete, OnInitialTilesAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvents.InitialTilesAnimationComplete, OnInitialTilesAnimationComplete);
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvents.InitialTilesAnimation);
        }

        private void OnInitialTilesAnimationComplete()
        {
            Complete();
        }
    }
}