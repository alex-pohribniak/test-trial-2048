﻿using Common.Enums;
using Common.Singletons;
using Common.Tasks;

namespace GameScene.Tasks
{
    public class UpdateInitialTileViewTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvents.UpdateInitialTileView);
            Complete();
        }
    }
}