﻿using System.Collections.Generic;
using System.Linq;
using Common.Singletons;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Models;
using UnityEngine;

namespace GameScene.Tasks
{
    public class UpdateInitialTileDataTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var initialTiles = Model.GameData.TilesData
                .Where(tile => tile.Value.TileValue == GameConfig.InitialTileValue).ToList();

            if (initialTiles.Count > 0)
            {
                var randomIndex = Random.Range(0, initialTiles.Count);
                var randomTile = initialTiles.ElementAt(randomIndex);
                Model.GameData.TilesData[randomTile.Key].TileValue *= GameConfig.Multiplier;

                var currentAnimations = new List<AnimationData>
                {
                    new AnimationData(
                        AnimationType.UpdateInitialTile,
                        GameConfig.ValueColorDictionary[Model.GameData.TilesData[randomTile.Key].TileValue],
                        GameConfig.ValueColorDictionary[Model.GameData.TilesData[randomTile.Key].TileValue],
                        Model.GameData.TilesData[randomTile.Key].TileValue,
                        Model.GameData.TilesData[randomTile.Key].TileValue,
                        randomTile.Key,
                        randomTile.Key
                    )
                };

                Model.GameData.AnimationsSequenceData.Add(Model.GameData.AnimationStep, currentAnimations);
                Model.GameData.AnimationStep++;
            }

            Complete();
        }
    }
}