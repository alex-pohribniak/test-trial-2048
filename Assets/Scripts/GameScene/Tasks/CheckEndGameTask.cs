﻿using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using Common.Singletons;
using Common.Tasks;
using GameScene.Config;
using GameScene.Models;
using UnityEngine;

namespace GameScene.Tasks
{
    public class CheckEndGameTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            foreach (var tileData in Model.GameData.TilesData)
                if (tileData.Value.TileValue == GameConfig.GoalToReachTileValue)
                {
                    Model.GameData.GoalReached = true;
                    Model.GameData.GameOver = false;
                    Observer.Emit(GameEvents.GoalReached);
                    Complete();
                }

            if (Model.GameData.TilesData.Values.All(tileData => tileData.TileValue != GameConfig.InitialTileValue))
            {
                var blockedTilesData = new List<CustomTileData>();
                for (var i = 0; i < GameConfig.GameBoardRows; i++)
                for (var j = 0; j < GameConfig.GameBoardRows; j++)
                {
                    var currentPosition = new Vector2Int(i, j);
                    var tileData = Model.GameData.TilesData[currentPosition];
                    var leftNeighbour = currentPosition + Vector2Int.left;
                    var rightNeighbour = currentPosition + Vector2Int.right;
                    var topNeighbour = currentPosition - Vector2Int.up;
                    var bottomNeighbour = currentPosition - Vector2Int.down;
                    var blocked = false;

                    if (Model.GameData.TilesData.ContainsKey(leftNeighbour))
                        if (tileData.TileValue != Model.GameData.TilesData[leftNeighbour].TileValue)
                            blocked = true;
                        else
                            continue;

                    if (Model.GameData.TilesData.ContainsKey(rightNeighbour))
                        if (tileData.TileValue != Model.GameData.TilesData[rightNeighbour].TileValue)
                            blocked = true;
                        else
                            continue;

                    if (Model.GameData.TilesData.ContainsKey(topNeighbour))
                        if (tileData.TileValue != Model.GameData.TilesData[topNeighbour].TileValue)
                            blocked = true;
                        else
                            continue;

                    if (Model.GameData.TilesData.ContainsKey(bottomNeighbour))
                        if (tileData.TileValue != Model.GameData.TilesData[bottomNeighbour].TileValue)
                            blocked = true;
                        else
                            continue;

                    if (blocked) blockedTilesData.Add(tileData);
                }

                if (blockedTilesData.Count == Model.GameData.TilesData.Count)
                {
                    Model.GameData.GoalReached = false;
                    Model.GameData.GameOver = true;
                    Observer.Emit(GameEvents.GameOver);
                    Complete();
                }
            }

            Complete();
        }
    }
}