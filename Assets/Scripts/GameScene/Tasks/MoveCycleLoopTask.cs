﻿using System.Collections.Generic;
using Common.Singletons;
using Common.Tasks;

namespace GameScene.Tasks
{
    public class MoveCycleLoopTask : BaseTask
    {
        private readonly SequenceTask _moveCycleTask;

        public MoveCycleLoopTask()
        {
            _moveCycleTask = new SequenceTask(new List<BaseTask>
            {
                new MoveLeftTask(),
                new MoveRightTask(),
                new MoveUpTask(),
                new MoveDownTask()
            });
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            _moveCycleTask.Execute();
            Complete();
        }

        protected override void Activate()
        {
            base.Activate();
            _moveCycleTask.onTaskComplete = OnCycleComplete;
            _moveCycleTask.onTaskGuard = OnCycleComplete;
        }

        private void OnCycleComplete(BaseTask task)
        {
            if (Model.GameData.Moved)
            {
                Model.GameData.MoreMovesAvailable = true;
                Model.GameData.Moved = false;
                Model.GameData.AnimationStep++;
            }
            else
            {
                Model.GameData.MoreMovesAvailable = false;
                foreach (var tileData in Model.GameData.TilesData) tileData.Value.Merged = false;
            }

            if (Guard()) _moveCycleTask.Execute();
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            _moveCycleTask.onTaskComplete = null;
            _moveCycleTask.onTaskGuard = null;
        }

        protected override bool Guard()
        {
            return Model.GameData.MoreMovesAvailable;
        }
    }
}