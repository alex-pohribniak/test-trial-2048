﻿using Common.Enums;
using Common.Singletons;
using Common.Tasks;

namespace GameScene.Tasks
{
    public class InitObjectPoolTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvents.InitObjectPool);
            Complete();
        }
    }
}