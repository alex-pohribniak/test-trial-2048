﻿using Common.Enums;
using Common.Singletons;
using Common.Tasks;

namespace GameScene.Tasks
{
    public class UnLockControlTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvents.UnLockControl);
            Complete();
        }
    }
}