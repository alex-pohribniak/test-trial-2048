﻿using System.Collections.Generic;
using Common.Singletons;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Models;
using UnityEngine;

namespace GameScene.Tasks
{
    public class MoveUpTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentAnimations = new List<AnimationData>();
            for (var i = 0; i < GameConfig.GameBoardRows; i++)
            for (var j = 0; j < GameConfig.GameBoardColumns; j++)
            {
                if (j == 0) continue;
                var previousPosition = new Vector2Int(j - 1, i);
                var previousTileData = Model.GameData.TilesData[previousPosition];
                var currentPosition = new Vector2Int(j, i);
                var currentTileData = Model.GameData.TilesData[currentPosition];

                if (previousTileData.TileValue != GameConfig.InitialTileValue &&
                    currentTileData.TileValue != GameConfig.InitialTileValue && previousTileData.Merged == false &&
                    currentTileData.Merged == false)
                {
                    if (previousTileData.TileValue != currentTileData.TileValue) continue;
                    previousTileData.TileValue *= GameConfig.Multiplier;
                    previousTileData.Merged = true;
                    currentTileData.TileValue = GameConfig.InitialTileValue;
                    Model.GameData.Moved = true;
                    currentAnimations.Add(new AnimationData(
                        AnimationType.Merge,
                        GameConfig.ValueColorDictionary[currentTileData.TileValue],
                        GameConfig.ValueColorDictionary[previousTileData.TileValue],
                        currentTileData.TileValue,
                        previousTileData.TileValue,
                        currentPosition,
                        previousPosition
                    ));
                }
                else if (previousTileData.TileValue == GameConfig.InitialTileValue &&
                         currentTileData.TileValue != GameConfig.InitialTileValue)
                {
                    previousTileData.TileValue = currentTileData.TileValue;
                    currentTileData.TileValue = GameConfig.InitialTileValue;
                    Model.GameData.Moved = true;
                    currentAnimations.Add(new AnimationData(
                        AnimationType.Move,
                        GameConfig.ValueColorDictionary[previousTileData.TileValue],
                        GameConfig.ValueColorDictionary[previousTileData.TileValue],
                        previousTileData.TileValue,
                        previousTileData.TileValue,
                        currentPosition,
                        previousPosition
                    ));
                }
            }

            Model.GameData.AnimationsSequenceData.Add(Model.GameData.AnimationStep, currentAnimations);

            Complete();
        }

        protected override bool Guard()
        {
            return Model.GameData.MoveDirection == Vector2Int.up;
        }
    }
}