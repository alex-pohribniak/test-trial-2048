﻿using Common.Enums;
using Common.Singletons;
using Common.Tasks;
using UnityEngine;

namespace GameScene.Tasks
{
    public class TilesAnimationTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvents.TilesAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvents.TilesAnimationComplete, OnTilesAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvents.TilesAnimationComplete, OnTilesAnimationComplete);
        }

        private void OnTilesAnimationComplete()
        {
            Complete();
        }
    }
}