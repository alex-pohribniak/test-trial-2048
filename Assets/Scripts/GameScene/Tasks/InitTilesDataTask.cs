﻿using System.Collections.Generic;
using Common.Singletons;
using Common.Tasks;
using GameScene.Config;
using GameScene.Models;
using UnityEngine;

namespace GameScene.Tasks
{
    public class InitTilesDataTask : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Model.GameData.TilesData = new Dictionary<Vector2Int, CustomTileData>();
            for (var x = 0; x < GameConfig.GameBoardRows; x++)
            for (var y = 0; y < GameConfig.GameBoardColumns; y++)
                Model.GameData.TilesData.Add(new Vector2Int(x, y),
                    new CustomTileData(false, GameConfig.InitialTileValue));
            Complete();
        }
    }
}