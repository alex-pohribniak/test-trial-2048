﻿namespace GameScene.Enums
{
    public enum AnimationType
    {
        UpdateInitialTile,
        Move,
        Merge,
    }
}