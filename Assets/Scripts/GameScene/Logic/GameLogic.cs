using System.Collections.Generic;
using Common.Singletons;
using Common.Tasks;
using GameScene.Models;
using GameScene.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameScene.Logic
{
    public class GameLogic : MonoBehaviour
    {
        public void Reset()
        {
            SceneManager.LoadSceneAsync("Scenes/GameScene");
        }

        private void Start()
        {
            InitGameBoard();
        }

        public void LoadMenuScene()
        {
            SceneManager.LoadSceneAsync("Scenes/MenuScene");
        }

        public static void InitGameBoard()
        {
            var initGameBoardTask = new SequenceTask(new List<BaseTask>
            {
                new InitObjectPoolTask(),
                new InitTilesDataTask(),
                new InitTilesViewTask()
            })
            {
                onTaskComplete = OnInitGameBoardComplete,
                onTaskGuard = OnInitGameBoardComplete
            };
            initGameBoardTask.Execute();
        }

        private static void OnInitGameBoardComplete(BaseTask task)
        {
            Model.GameData.AnimationStep = 0;
            Model.GameData.AnimationsSequenceData = new Dictionary<int, List<AnimationData>>();
            var updateInitialTileDataTask = new SequenceTask(new List<BaseTask>
            {
                new LockControlTask(),
                new UpdateInitialTileDataTask(),
                new UpdateInitialTileDataTask(),
                new DelaySecondsTask(0.1f),
                new InitialTilesAnimationTask(),
                new UpdateInitialTileViewTask(),
                new UnLockControlTask()
            });
            updateInitialTileDataTask.Execute();
        }

        public static void MoveAction(Vector2Int direction)
        {
            Model.GameData.MoveDirection = direction;
            Model.GameData.MoreMovesAvailable = true;
            Model.GameData.AnimationStep = 0;
            Model.GameData.AnimationsSequenceData = new Dictionary<int, List<AnimationData>>();
            var moveActionTask = new SequenceTask(new List<BaseTask>
            {
                new LockControlTask(),
                new MoveCycleLoopTask(),
                new TilesAnimationTask()
            })
            {
                onTaskComplete = OnMoveActionComplete,
                onTaskGuard = OnMoveActionComplete
            };
            moveActionTask.Execute();
        }

        private static void OnMoveActionComplete(BaseTask task)
        {
            Model.GameData.AnimationStep = 0;
            Model.GameData.AnimationsSequenceData = new Dictionary<int, List<AnimationData>>();
            var updateInitialTileDataTask = new SequenceTask(new List<BaseTask>
            {
                new LockControlTask(),
                new CheckEndGameTask(),
                new UpdateInitialTileDataTask(),
                new InitialTilesAnimationTask(),
                new UpdateInitialTileViewTask(),
                new UnLockControlTask()
            });
            updateInitialTileDataTask.Execute();
        }
    }
}