﻿using GameScene.Enums;
using UnityEngine;

namespace GameScene.Models
{
    public class AnimationData
    {
        public AnimationType AnimationType;
        public Color StartColor;
        public Color TargetColor;
        public int StartValue;
        public int TargetValue;
        public Vector2Int StartPosition;
        public Vector2Int TargetPosition;

        public AnimationData(AnimationType animationType, Color startColor, Color targetColor, int startValue, int targetValue, Vector2Int startPosition, Vector2Int targetPosition)
        {
            AnimationType = animationType;
            StartColor = startColor;
            TargetColor = targetColor;
            StartValue = startValue;
            TargetValue = targetValue;
            StartPosition = startPosition;
            TargetPosition = targetPosition;
        }
    }
}