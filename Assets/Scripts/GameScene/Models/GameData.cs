﻿using System.Collections.Generic;
using UnityEngine;


namespace GameScene.Models
{
    public class GameData
    {
        public bool MoreMovesAvailable;
        public bool Moved;
        public bool GoalReached;
        public bool GameOver;
        public Vector2Int MoveDirection;
        public Dictionary<Vector2Int, CustomTileData> TilesData;
        public int AnimationStep;
        public Dictionary<int,List<AnimationData>> AnimationsSequenceData;
    }
}