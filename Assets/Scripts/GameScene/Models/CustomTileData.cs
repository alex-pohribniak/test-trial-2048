﻿namespace GameScene.Models
{
    public class CustomTileData
    {
        public bool Merged;
        public int TileValue;

        public CustomTileData(bool merged, int tileValue)
        {
            Merged = merged;
            TileValue = tileValue;
        }
    }
}