﻿using GameScene.Logic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameScene.Controller
{
    public class Controller : MonoBehaviour, IDragHandler
    {
        public Vector2Int moveDirection;

        public void OnDrag(PointerEventData eventData)
        {
            if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
                moveDirection = eventData.delta.x > 0f ? Vector2Int.right : Vector2Int.left;
            else
                moveDirection = eventData.delta.y > 0f ? Vector2Int.up : Vector2Int.down;

            GameLogic.MoveAction(moveDirection);
        }
    }
}