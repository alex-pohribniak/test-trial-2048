using System.Collections.Generic;
using UnityEngine;

namespace GameScene.Config
{
    public static class GameConfig
    {
        public const int GameBoardRows = 5;
        public const int GameBoardColumns = 5;
        public const int GoalToReachTileValue = 8192;
        public const int InitialTileValue = 1;
        public const int Multiplier = 2;
        public const float AnimationDuration = 0.1f;

        public static readonly Dictionary<int, Color> ValueColorDictionary = new Dictionary<int, Color>
        {
            {1, new Color(0.83f, 0.8f, 0.76f, 1f)},
            {2, new Color(1f, 0.99f, 0.9f, 1f)},
            {4, new Color(1f, 0.97f, 0.76f, 1f)},
            {8, new Color(1f, 0.96f, 0.61f, 1f)},
            {16, new Color(1f, 0.94f, 0.46f, 1f)},
            {32, new Color(1f, 0.93f, 0.34f, 1f)},
            {64, new Color(1f, 0.92f, 0.23f, 1f)},
            {128, new Color(1f, 0.84f, 0.2f, 1f)},
            {256, new Color(0.98f, 0.75f, 0.17f, 1f)},
            {512, new Color(0.97f, 0.65f, 0.14f, 1f)},
            {1024, new Color(0.96f, 0.56f, 0f, 1f)},
            {2048, new Color(0.96f, 0.49f, 0.09f, 1f)},
            {4096, new Color(0.96f, 0.43f, 0f, 1f)},
            {8192, new Color(0.9f, 0.3f, 0f, 1f)}
        };
    }
}